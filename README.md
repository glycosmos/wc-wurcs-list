# Components

GitLab CI works on this project so check the Gitlab [Page]().

# Tutorial

Install node packages.
```
$ npm i
```

A main webcomponent file is in dist/ directory. To update dist, enter webpack command.
```
$ webpack
```

Open the index on a browser
```
$ npm run start
```
Project is running at http://localhost:8080/
