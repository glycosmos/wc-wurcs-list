import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@granite-elements/granite-bootstrap/granite-bootstrap.js';

class RepoWurcsList extends PolymerElement {
  static get template() {
    return html`
<style include="granite-bootstrap">
/* shadow DOM styles go here */:

</style>

  <iron-ajax
    auto url="https://test.sparqlist.glycosmos.org/sparqlist/api/wurcs_select_valid_wurcs_list"
    handle-as="json"
    last-response="{{sampleids}}"></iron-ajax>

  <div class="table-responsive">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Hash</th>
          <th>Input text</th>
          <th>WURCS</th>
          <th>Warning</th>
          <th>Error</th>
        </tr>
      </thead>
      <tbody>
        <template is="dom-repeat" items="{{sampleids}}">
          <tr>
            <td>[[item.hash]]</td>
            <td>[[item.input_text]]</td>
            <td>[[item.valid_wurcs]]</td>
            <td>[[item.warning_message]]</td>
            <td>[[item.error_message]]</td>
          </tr>
        </template>
      </tbody>
    </table>
  </div>
   `;
  }
  constructor() {
    super();
  }
  static get properties() {
    return {
      sampleids: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      }
    };
  }

  _handleAjaxPostResponse(e) {
    console.log(e);
  }
  _handleAjaxPostError(e) {
    console.log('error: ' + e);
  }
}

customElements.define('repo-wurcslist', RepoWurcsList);
